# ![local-it](assets/logo_large.png)

## Software für kollaboratives Arbeiten [SKA]

Daten werden zum wichtigsten Gut des 21.Jahrhunderts. Daher ist es umso wichtiger zu wissen wo sie gespeichert sind, wer darauf Zugriff hat und welche Systeme diese verwalten.

Ziel von „SkA“ – **S**oftwaretools für **k**ollaboratives **A**rbeiten ist es, Bürger:innen, Projektgruppen, Vereinen oder Organisationen vorhandene Open-Source Apps technisch einfach zugänglich zu machen und sie zu befähigen diese Apps zu nutzen. Gleichzeitig das Zusammenwirken der verschiedenen Apps untereinander komfortabler zu gestalten damit diese mit etablierten properitären Apps mithalten können


### Tool Übersicht
[Liste mit kollaborativen Tools](tools/README.md)


### Installation

[Installation](installation/)

Apps:

* [Authentik](installation/authentik.md)
* [Nextcloud](installation/nextcloud.md)
* [OnlyOffice](installation/onlyoffice.md)
* [Wekan](installation/wekan.md)


### [Frequently Asked Questions](./faq.md)

---

Ein Projekt von [local-it](https://local-it.org)