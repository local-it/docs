# Nextcloud

```
abra app new nextcloud
abra config cloud.example.org
abra deploy cloud.example.org
```

Die Installation von Nextcloud kann eine Weile dauern. Der status kann so überprüft werden:
`abra app run --user www-data cloud.example.org app /var/www/html/occ status`

#### Installation von Nextcloud Apps:

```
abra app command cloud.example.org app install_apps calendar sociallogin onlyoffice
```

#### Dashboard deaktivieren:

`abra app run --user www-data cloud.example.org app /var/www/html/occ app:disable dashboard`

#### BBB Integration

https://github.com/sualko/cloud_bbb/

```
abra app command cloud.example.org app install_bbb https://bbb.example.org/bigbluebutton/ <secret-key>
```

#### Installation von Nextcloud Apps:

```
abra app config cloud.example.org
    APPS="calendar sociallogin onlyoffice"
abra app cmd cloud.example.org app install_apps
```

#### Dashboard deaktivieren:

`abra app cmd cloud.example.org app run_occ "'app:disable dashboard'"`

#### Nextcloud [Social Login SSO](https://github.com/zorn-v/nextcloud-social-login/tree/54ff473bca76bae891fbcd0d6f0fa6a937e6c1ab) konfigurieren

im authentik:
openid-provider anlegen
app anlegen
redirect uri: https://cloud.example.org/apps/sociallogin/custom_oidc/authentik

```
abra app run --user www-data cloud.example.org app /var/www/html/occ config:app:set sociallogin custom_providers --value='
{
  "custom_oidc": [
    {
      "name": "authentik",
      "title": "SKA",
      "authorizeUrl": "https://sso.example.org/application/o/authorize/",
      "tokenUrl": "https://sso.example.org/application/o/token/",
      "displayNameClaim": "preferred_username",
      "userInfoUrl": "https://sso.example.org/application/o/userinfo/",
      "logoutUrl": "https://sso.example.org/application/o/nextcloud/end-session/",
      "clientId": "nextcloud",
      "clientSecret": "secret",
      "scope": "openid profile email nextcloud",
      "groupsClaim": "nextcloud_groups",
      "style": "openid",
      "defaultGroup": "",
      "groupMapping": {
        "admin": "admin"
      }
    }
  ]
}'


abra app run --user www-data cloud.example.org app /var/www/html/occ config:app:set sociallogin update_profile_on_login --value 1
abra app run --user www-data cloud.example.org app /var/www/html/occ config:app:set sociallogin auto_create_groups --value 1
abra app run --user www-data cloud.example.org app /var/www/html/occ config:app:set sociallogin hide_default_login --value 1

All Possbile Values:
        'disable_registration',
        'create_disabled_users',
        'allow_login_connect',
        'prevent_create_email_exists',
        'update_profile_on_login',
        'no_prune_user_groups',
        'auto_create_groups',
        'restrict_users_wo_mapped_groups',
        'restrict_users_wo_assigned_groups',
        'disable_notify_admins',
        'hide_default_login',


abra app run --user www-data cloud.example.org app /var/www/html/occ config:system:set allow_user_to_change_display_name --value=false
abra app run --user www-data cloud.example.org app /var/www/html/occ config:system:set lost_password_link --value=disabled
abra app run --user www-data cloud.example.org app /var/www/html/occ config:system:set social_login_auto_redirect --value=true

```

Konfiguration überprüfen

```
abra app run --user www-data cloud.example.org bash
cat config/config.php
```

### Custom Group Mapping

In Authentik:

- Customisation -> Property Mappings
  - Create -> Scope Mapping

Scope-Name: "nextcloud"
Expression: `return {	"nextcloud_groups": [{"gid": group.name, "displayName": group.name} for group in request.user.ak_groups.all()], }`

- Providers -> nextcloud
  - Advanced Protocol Settings
    - Scopes

Scopes auswählen: "authentik default OAuth Mapping: OpenID {email, openid, profile}" , nextcloud

## Backup

### Nextcloud von einem Backup wiederherstellen

Offizieller [Nextcloud-Guide](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/restore.html)

Wartungs-Modus aktivieren

```
abra app run --user www-data yksflip-cloud app /var/www/html/occ maintenance:mode --on
```

Wiederherstellen von Dateien

```
restic restore --include /backups/yksflip-cloud_app -t restored latest
docker cp -a r/backups/yksflip-cloud_app/* $(docker ps -q -f name=cloud_app):/
```

Sicher stellen dass File-Permissions und Ownership stimmen:

```
docker exec -it $(docker ps -q -f name=cloud_app) bash
$ ls -la
$ chown -R www-data:www-data data config custom_apps
```

Datenbank wiederherstellen

```

restic restore --include /backups/yksflip-cloud_db -t r latest
docker exec -it  $(docker ps -q -f name=cloud_db) bash
$ mysql -u root -p"$(cat /run/secrets/db_root_password)"  -e "DROP DATABASE nextcloud"
$ mysql -u root -p"$(cat /run/secrets/db_root_password)"  -e "CREATE DATABASE nextcloud"
$ mysql -u root -p"$(cat /run/secrets/db_root_password)" nextcloud < /tmp/backup/backup.sql
```

!!! note "Bei Postgres DB"

    ```
    PGPASSWORD="password" psql -h [server] -U [username] -d template1 -c "DROP DATABASE \"nextcloud\";"
    PGPASSWORD="password" psql -h [server] -U [username] -d template1 -c "CREATE DATABASE \"nextcloud\";"
    /usr/bin/pg_restore -c -U nextcloud -d nextcloud < /tmp/backup/dump.sql
    ```

git

Wartungs-Modus aktivieren

```
abra app run --user www-data yksflip-cloud app /var/www/html/occ maintenance:data-fingerprint
abra app run --user www-data yksflip-cloud app /var/www/html/occ maintenance:mode --on
```

Falls nötig kann auch ein repair gestartet werden:

```
abra app run --user www-data yksflip-cloud app /var/www/html/occ  maintenance:repair
```
