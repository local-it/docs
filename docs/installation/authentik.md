---
title: Single-Sign-On Provider
---

# Single-Sign-On Provider

[Authentik](https://goauthentik.io) ist unser Single-Sign-On (SSO) Provider und Identity Management.
Alle Apps die per SSO angebunden werden sollen, müssen später in der Administrationsoberfläche konfiguriert werden.

Setup: [https://git.coopcloud.tech/coop-cloud/authentik](https://git.coopcloud.tech/coop-cloud/authentik)
