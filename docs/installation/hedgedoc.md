# Hedgedoc

Setup: [https://git.coopcloud.tech/coop-cloud/hedgedoc](https://git.coopcloud.tech/coop-cloud/hedgedoc)

## SKA Spezifische Konfiguration

Authentik SSO:

- Erstelle OAuth2 Provider
    - wähle `default-provider-authorization-implicit-consent`
- Erstelle Application
    - Start URL: <hedgedoc_domain>

Setze folgende Konfiguration mit `abra app YOURAPPDOMAIN config` und ersetze `<authentik_domain>` und `<client_id>`:
```
COMPOSE_FILE="$COMPOSE_FILE:compose.oauth.yml"
CMD_OAUTH2_PROVIDERNAME=local-it
CMD_OAUTH2_CLIENT_ID=<client_id>
CMD_OAUTH2_AUTHORIZATION_URL=https://<authentik_domain>/application/o/authorize/
CMD_OAUTH2_TOKEN_URL=https://<authentik_domain>/application/o/token/
CMD_OAUTH2_USER_PROFILE_URL=https://<authentik_domain>/application/o/userinfo/
CMD_OAUTH2_USER_PROFILE_USERNAME_ATTR=preferred_username
CMD_OAUTH2_USER_PROFILE_DISPLAY_NAME_ATTR=name
CMD_OAUTH2_USER_PROFILE_EMAIL_ATTR=email
CMD_OAUTH2_SCOPE="openid email profile"

SECRET_OAUTH_KEY_VERSION=v1
```

Füge das client secret aus Authentik ein:

`abra app secret insert pad.kaputt.cloud oauth_key v1 <authentik_secret>`

Weitere Optionen:

```
CMD_ALLOW_ANONYMOUS=true
CMD_ALLOW_FREEURL=true
CMD_COOKIE_POLICY=none
```
