---
title: Installation
---

# Installation

Unser Setup basiert auf [abra](https://git.coopcloud.tech/coop-cloud/abra) von [coopcloud](https://coopcloud.tech)


## Voraussetzungen

* SSH-Zugang zu einem Server
  z.B. VPS (Virtual-Private-Server 2vCPU; 4GB RAM; 200GB Speicher) von windcloud.de
* öffentliche IPv4 Adresse
* Domain mit Zugriff zur DNS Einstellung:
  A auf IPv4
  *.example.org CNAME auf example.org

oder falls wildcard nicht möglich cnames für alle apps anlegen (cloud.example.org, board.example.org, office.example.org)


## Basis System

### Docker auf dem Server

```
ssh -p 22 root@example.org 
sudo apt update
sudo apt upgrade -y
sudo apt install docker.io
docker swarm init
docker network create -d overlay --scope swarm proxy
```
oder folge der Anleitung von [Docker](https://docs.docker.com/engine/install/)

### Abra auf dem Client

```
sudo apt update
sudo apt install curl git
curl https://install.abra.autonomic.zone | bash
export PATH=$PATH:$HOME/.local/bin
echo "PATH$PATH:$HOME/.local/bin" >> ~/.bashrc
abra -h
```

Server zu Abra hinzufügen

```
abra server add example.org username port
abra server init
```

Für weitere Details: [docs.coopcloud.tech](https://docs.coopcloud.tech/deploy/)

---

## Reverse-Proxy

Wir verwenden Traefik als Reverse-Proxy. Er erkennt automatisch Apps im Docker Swarm und leitet von den konfigurierten Subdomains auf die entsprechenden Apps um.

``` 
abra app new traefik
abra app config example_traefik # only if custom config needed 
abra app deploy example_traefik 
```

Du kannst den Status der Installation mit `abra app ps example_traefik` überprüfen


## Apps

Grundsätzlich können alle Apps verwendet werden, die von coop-cloud unterstützt werden, allerdings sind nicht alle Apps umbedingt vollständig integriert. Auf unserer Setie stellen wir Apps vor, die bereits mit Single-Sign-On integriert und wir im Betrieb schon gute Erfahrungen gemacht haben.


## Backups

Das automatisierte Backup basiert auf dem [backup-bot](https://git.coopcloud.tech/coop-cloud/backup-bot-two),
der anhand von Docker Labels Verzeichnisse kopiert und mit [Restic](https://restic.readthedocs.io/en/latest/) per S3 Storage oder per SFTP auf einer entfernten Maschine sichert. Von uns bereitgestellte Apps enthalten bereits die dafür benötigten Labels.

Voroab wird ein zugang zu einem S3-Kompatiblen Storage oder Server mit SFTP benötigt.
z.B.: [Hetzner Storage Box](https://www.hetzner.com/storage/storage-box), [Wasabi S3](wasabisys.com)

### Installation

#### S3 Storage

```
abra app new backup-bot-two
abra app secret insert servername-backupbot aws_secret_access_key v1 <secret-key>
abra app secret generate backupbot_local restic_password v1
abra app config
abra app deploy
```
#### SFTP Storage

TODO

### Wiederherstellen

```
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export RESTIC_REPOSITORY=s3:your-s3-storage.org:/bucketname
export RESTIC_PASSWORD_FILE=./restic_password

# Show list of all snaphosts
restic snapshots

# Show all paths in snapshot
restic ls latest

# Prune Snapshots 
restic forget  --prune --keep-daily 7 --keep-weekly 4  --keep-monthly 12 --keep-yearly 3

# Restore
restic restore --include /backups/servername-appname_servicename -t destination_folder latest
```


--8<-- "includes/abbreviations.md"