BigBlueButton
=============

# Big Blue Button

## Installation

Wir verwenden das [BBB Docker](https://github.com/bigbluebutton/docker/) Setup.

```
git clone --recurse-submodules https://github.com/bigbluebutton/docker.git bbb-docker
cd bbb-docker
git checkout develop
./scripts/setup
nano .env
./scripts/generate-compose
docker-compose up -d
```

### Admin Account erstellen

`docker-compose exec greenlight bundle exec rake admin:create`

### Nextcloud App Konfiguration anpassen:

```
abra app config <nextcloud_domain>
    BBB_URL=https://talk.example.org/bigbluebutton/
abra app secret insert <nextcloud_domain> bbb_secret v1 <bbb_secret>
abra app cmd <nextcloud_domain> app install_bbb
```

### Telefoneinwahl

Account mit Rufnummer bei https://app.sipgate.com
`./conf/dialplan_public`
`./conf/sip_profiles`
im .env noch: `SIP_IP_ALLOWLIST`, `WELCOME_FOOTER`


## weitere Ressourcen

* [Bandbreiten Rechner](https://bbb-hilfe.de/bandbreitenrechner-fuer-bigbluebutton/)
