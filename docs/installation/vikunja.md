# Vikunja

[Vikunja](https://vikunja.io/docs/) ist ein Kanban Board.


```
abra app new vikunja
abra config <app_name>
abra deploy <app_name>
```

## SSO

im authentik:

- openid-provider anlegen
    - Notiere: `<authentik client id>` und `<authentik secret>`
    - Signaturschlüssel auswählen.
- app anlegen


```
abra app secret insert <app_name> oauth_secret v1 <authentik secret>
abra config <app_name>
    OAUTH_URL=https://<domain>/application/o/vikunja/
    OAUTH_CLIENT_ID=<authentik client id>
```

