

### migration

https://www.keycloak.org//docs/latest/upgrading/index.html#_upgrading

https://www.keycloak.org//docs/latest/upgrading/index.html#migration-changes

https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/export-import.html

export with:
dc run app -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=/opt/jboss/export-2022-05-18.json


import:
copy export.json to volume folder
extend compose.yml with  command: "-Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=/opt/jboss/keycloak/themes/export.json -Dkeycloak.migration.strategy=OVERWRITE_EXISTING"

copy theme to theme volume
set welcome_theme env variable



