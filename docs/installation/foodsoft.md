Foodsoft
--------

https://github.com/foodcoops/foodsoft

### Installation

[Coop-Cloud Recipe](https://git.coopcloud.tech/coop-cloud/foodsoft)

### Migration

1. Neue instanz aufsetzen
2. Backup aus alter instanz erstellen

```bash
docker exec foodcoops_mariadb_1 bash -c "mysqldump -ufoodsoft -p${DB_PASSWORD} foodsoft_demo" > 2022-02-27.sql
```

3. Backup einspielen

```bash
➜ docker cp 2022-01-04.sql wandelgut-foodsoft_db.1.b1nspplwa49o55pwj5ddcnn0r:/tmp/
➜ docker exec -it wandelgut-foodsoft_db.1.b1nspplwa49o55pwj5ddcnn0r bash
root@dcdca844b2f1:/# cd /tmp/
root@dcdca844b2f1:/tmp# mysql -p$(cat /run/secrets/db_root_password)
MariaDB [(none)]> create database tantewandel;
root@dcdca844b2f1:/tmp# mysql -p$(cat /run/secrets/db_root_password) tantewandel < 2022-01-04.sql
root@dcdca844b2f1:/tmp# exit
➜ docker exec -it wandelgut-foodsoft_app.1.zvlhnq4vvmlf4iuck7ymz96xu bash
nobody@22e376ecea83:/usr/src/app$ SECRET_KEY_BASE=$(cat /run/secrets/secret_key_base) bundle exec rake db:migrate
```


### Updating Group Order Invoice Fork

1. `git clone https://github.com/foodcoops/foodsoft.git`
2. checkout pr
3. build image:
    `docker -t me/mytag:v123 .`
    `docker push me/mytag:v123`
4. update the image tag in the `~/.abra/apps/foodsoft/compose.groupOrderInvoice.yml` file
5. run `abra app deploy --chaos foodsoft-dev`
6. check status with:
  `abra app ps foodsoft-dev`
  `abra app logs foodsoft-dev`
7. if everything looks good, git commit & push