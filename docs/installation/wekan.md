# Wekan

[Wekan](https://github.com/wekan/wekan/wiki) ist ein Kanban Board.


```
abra app new wekan
abra app secret generate -A app_name
abra config app_name
abra deploy app_name
```

im authentik:
    openid-provider anlegen
        id: wekan
        geheminis: secret von abra
        redirect uris: https://board.example.org/_oauth/oidc
    app anlegen



Login Button umbenennen: User -> Administration -> Layout -> Benutzerdefinierter Text der OIDC-Schaltfläche

## SSO

create scope wekan
and attribute mapper with:

```
groupsDict = {"wekanGroups": []}
for group in request.user.ak_groups.all():
  my_attributes = group.attributes
  my_attributes["displayName"] = group.name
  my_attributes["isAdmin"] = group.attributes["isAdmin"] if 'isAdmin' in group.attributes else False
  my_attributes["isActive"] = group.attributes["isActive"] if 'isActive' in group.attributes else True
  my_attributes["forceCreate"] = group.attributes["forceCreate"] if 'forceCreate' in group.attributes else True
  groupsDict["wekanGroups"].append(my_attributes)
return groupsDict
```

see https://github.com/wekan/wekan/tree/master/packages/wekan-oidc


## Backup

### Wekan von einem Backup wiederherstellen

TODO restic stuff

```
docker context use old-server
docker exec wekan_db_1 bash -c "mongodump --archive=/tmp/wekan.archive"
docker cp wekan_db_1:dump/wekan-2021-12-03.archiv .
docker context use new-server
docker cp wekan-2021-12-03.archiv lit-board_db.1.yzvn5bhlr4vpval2kytazhgwr:/tmp/
docker exec lit-board_db.1 bash -c "mongorestore --drop --verbose --archive=/tmp/wekan.archive"
```


#

https://www.mongodb.com/developer/products/mongodb/cheat-sheet/


make user admin
```
mongo
use wekan
db.users.find({"username": "philipp"})
db.users.update({"_id": "JwMP7y8QJyEd6r9F4"}, {$set: {"isAdmin" : true})
```