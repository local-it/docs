
```
abra app new gitea
abra app config
abra app deploy git.example.org
abra app config proxy.example.org 
# enable the gitea ssh port forward
```



```
docker exec --user git  gitea_server_1 gitea 
    admin auth add-oauth  
    --name keycloak 
    --provider openidConnect 
    --key gitea 
    --secret ${OAUTH_SECRET} 
    --auto-discover-url https://login.example.org/auth/realms/master/.well-known/openid-configuration
```


## backup / migration stuff

https://docs.gitea.io/en-us/backup-and-restore/


### create dump


```
docker exec -it -u git -w /tmp/tmp.dHKOjb/ gitea_app_1 bash -c '/app/gitea/gitea dump -c /data/gitea/conf/app.ini'
docker cp gitea_app_1:/tmp/tmp.dHKOjb/  .
```


### restore from backup

transfer secrets from app.ini to abra secret insert

```
unzip gitea-dump-1234.zip

# app
abra app cp git.example.org gitea-dump-1234.zip app:/tmp
abra app run -u git git.example.org app bash
$ cd /tmp && unzip gitea-dump-1234.zip
$ mv data /var/lib/gitea/data
$ mv repos /var/lib/gitea/git/repositories

# db
abra app cp git.example.org gitea-db.sql db:/tmp
abra app run git.example.org db bash
$ psql -U gitea -d gitea < /tmp/gitea-db.sql
```

gitea doctor --fix --run fix-broken-repo-units

