---
title: Rallly
---

# Rallly

[Rallly](https://github.com/lukevella/rallly) ist unsere Terminumfrage Software.

Setup: [https://git.coopcloud.tech/coop-cloud/rallly](https://git.coopcloud.tech/coop-cloud/rallly)
