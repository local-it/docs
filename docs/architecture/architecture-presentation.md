## Architekturkriterien

* Modularität
* Einfache, Paketbasierte Installation
* Überprüfbarkeit (Monitoring, Tests)
* Security und Datensicherheit
* Datenschutz (Logging, DSGVO)
* Federation und Verknüpfung mit anderen Diensten
* Freie Software

## Anforderungen an Apps

* Cloud-native
    * Container
    * Konfiguration per Umgebungsvariable
    * Health- / Monitoring-Endpoint
* Single-Sign-On
* API-Zugriff
* Aktive Entwicklung (mind. security updates)
* FOSS (Free and Open Source) Lizenz


## System Überblick

![](system-view.png)

## Design Decisions

### Containerisierung 
Docker:whale: 

### Single-Sign-On
Openid Connect (oAuth2)

### Automatisierung
Abra (coop-cloud)

### Monitoring
WiP (Grafana, Prometheus, CAdvisor)

### Backup
Backupbot (automated volume backup with Restic)

### Reverse Proxy
traefik