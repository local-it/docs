# FAQ

## Allgemeine Design Decisions

???+ faq "Warum Opensource?"
    * kein Vendor-Lockin
    * Weltweite Softwareentwicklung
    * Nachweisbare software qualität
    * Individualisierbar
    * Sicherer (mehr Augen)
    * Community
    * Public Money, Public Code
    * Use, Study, Share, Improve 

??? faq "Warum selbst hosten?"
    * Datenhohheit
    * Souveränität

??? faq " Warum openid-connect?"
    * moderner Offener Standard
    * flexible anbindung von apps
      sso session nicht auf eine domäne begrenzt
  
??? faq "Warum Containerisieren?"
    * unterschiedliche Apps, unterschiedliche Bedürfnisse
    * Isolation (Security)
    * Update einfacher
    * trennung von image - volumes
    * besser skalieren 

??? faq " Container vs VM"
    * weniger ressourcen, schneller
      (container enhält nur das was benötigt wird, gleiche images sparen speicher)
    * leichter zu orchestrieren

??? faq "Warum so viele einzelne DBs?"
    * Unterschiedliche bedarfe Postgres/Mysql/Mongo
    * in unterschiedlichen Versionen
    * leicht zu orchestrieren
  
??? faq "Warum nicht Kubernetes?"
    * Zu komplex
    * single node systeme für unsere zielgruppe

## Server Operator

### Authentik 

??? failure "Ich habe ausversehen meinen Admin Account von der Admin Gruppe entfernt"
    `abra app run sso.example.org server ak create_admin_group admin`   
    [see docs](https://goauthentik.io/docs/troubleshooting/missing_admin_group)

    * hohe ressourcen verbrauch