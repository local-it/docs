[Source](https://github.com/joelparkerhenderson/architecture-decision-record/blob/main/templates/decision-record-template-madr/index.md)

# Wie orchestrieren wir unsere Container

* Status: accepted
* Deciders: philipp
* Date: 2021

Technical Story: [description | ticket/issue URL] <!-- optional -->

## Context and Problem Statement

Betrieb von freier Software Webdienste (z.B. Nextcloud).

Wie können wir Dienste in einem einheitlichen best-practice setup  automatisiert deployen? Dabei sollen aber pro Instanz Konfigurationen möglichst flexibel bleiben. 

Außerdem Routineaufgaben sollten leicht zu erledigen sein
  * Aufsetzen neuer Dienste
  * Versions Upgrade
  * Konfigurationsänderungen
  * Backup u. Restore

## Decision Drivers <!-- optional -->

* Freie Software
* Einarbeitungszeit
* Geringe Komplexität
* Geringer Wartungsaufwand für Konfigurationen
* Nachhaltig, Langlebig
* Separierung (Security)
* Modularität (Unabhängigkeit)


## Considered Options

* YunoHost
* Cloudron
* Stackspin (Kubernetes)
* docker-swarm
* ansible
* pyinfra
* coop-cloud + abra
* 

## Decision Outcome

Coop-Cloud und Abra, wegen geringer Einarbeitungszeit und Komplexität, weil viele Architekturentscheidungen (Reverseproxy, Netzwerk, Conventions) schon vorgegeben sind. App deployments (Recipes) werden gemeinschaftlich entwickelt, es wird soweit möglich die upstream container verwendet, das verringert den Wartungsaufwand. Die Container ermöglichen eine Separierung und Modularität zwischen Diensten. 

Wir gehen davon aus, dass Docker-Swarm bestehen bleibt oder von der Community weiterentwickelt wird.
Selbst wenn Coop-Cloud nicht fortgeführt wird, ist mit Docker-Swarm soweit Rückwärts kompatibel, dass wir damit weitermachen könnten.


### Positive Consequences <!-- optional -->

* Wir können dran weiterentwickeln
* Welcoming Community
* Recipes können auch ohne abra weiterverwendet werden 
* Viel Gestaltungsfreiraum
* Einfacher Zugriff auf Logs

### Negative Consequences <!-- optional -->

* Abra noch in Beta, viele Bugs!
* CLI nicht so zugänglich wie z.B. Yunohost, Cloudron
* Pro-Instanz Anpassungen sind durch .env nicht so flexibel 

## Pros and Cons of the Options <!-- optional -->

### Ansible

* :heavy_plus_sign:  Roles für die meisten Apps vorhanden
* :heavy_plus_sign:  große Community, weit verbeitet
* :heavy_plus_sign:  host konfiguration
* :heavy_minus_sign: Schwieriger zu lernen 
* :heavy_minus_sign: nur für un-/deployment geeignet, z.B. kein docker ps?
    isolation, backup, upgrades
* :heavy_minus_sign: versionierung?

### Yunohost

* :heavy_plus_sign:  Administraion UI
* :heavy_plus_sign:  Viele Apps supported
* :heavy_plus_sign:  Integrierter Mail Server
* :heavy_plus_sign:  LDAP Anbindung
* :heavy_plus_sign:  Backup, Deploy, CI, Testing
* :heavy_minus_sign: Kein SSO
* :heavy_minus_sign: Keiner Containerisierung
* :heavy_minus_sign: Schwierig mehrere Instanzen zu verwalten
* :heavy_minus_sign: Einrichten pro Instanz über UI
* :heavy_minus_sign: Mehr als Selfhosting gedacht

### Stackspin 

* :heavy_plus_sign:  Mehr orchestrierungsfeatures von k8s
* :heavy_plus_sign:  Skalierbarkeit? (bisher nur single-node)
* :heavy_plus_sign:  SSO bereits automatsiert als k8s configs
* :heavy_minus_sign: Usermgmt Oberfläche fehlen noch viele Features
* :heavy_minus_sign: hoher Ressourcenverbrauch durch k8s
* :heavy_minus_sign: Erstellen neuer Recipes komplex  
* :heavy_minus_sign: Installation noch recht komplex


## Links <!-- optional -->

* [Link type] [Link to ADR] <!-- example: Refined by [ADR-0005](0005-example.md) -->
* … <!-- numbers of links can vary -->
