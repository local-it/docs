# Architecture Decisions

## Infrastructure

- Deployment
    - [orchestration](./Infrastructure/orchestration.md)
    - [VM management](./Infrastructure/VM_management.md)
    - [VM deployment](./Infrastructure/VM_deployment.md)

- Backup
    - [VM Backups](./Infrastructure/VM_Backups.md)
    - [Volume Backups](./Infrastructure/Volume_Backups.md)

- [Monitoring](./Infrastructure/Monitoring.md)

- [Issue Tracking](./Infrastructure/Issue_Tracking.md)


## Applications

- [SSO](./Applicatons/sso.md)

- [Filestorage](./Applicatons/file_storage.md)

- [Calendar](./Applicatons/calendar.md)

- [Chat](./Applicatons/chat.md)

- [video conference](./Applicatons/video_conference.md)

- [Office](./Applicatons/Office.md)

- [Notes](./Applicatons/Notes.md)

- [Project management](./Applicatons/project_management.md)
