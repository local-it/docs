---
title: "2022-09-07 Zurück Button, Dashboard"
---


Datum: 2022-09-07

Beteiligt: pf, pr, jh, mh

[Issue](https://git.local-it.org/local-it/ska/issues/5)


Infos
-----

??? example "fork"

    pros:

    * theming teilsweise auch als plugin möglich (nextcloud)
    * evtl. müssen wir das eh irgendwann, wenn wir sehr viel ui anpassen wollen?
    * meiste kontrolle
    * müssen vllt trotzdem nen fork maintainen

    cons:

    * Fork maintainen (upgrades, ...) (kann aber vllt automatisiert werden?)
    * Einarbeitungsaufwand in alle Apps


??? example "iframe"


    pros:

    * (perspektivisch frontend, das viel mehr werden kann)
    * relativ geringer aufwand

    cons:

    * x-frame-options headers, csp
      müssen in allen apps gesetzt werden (recipes anpassen)
    * share-links (z.B nextcloud) referenzieren nicht auf dashboard. sondern auf app direkt, dh. kein Zurückbutton mehr
      url kann nicht gebookmarkt werden
      (vllt über reverse-proxy hacken?)
    * nicht so einfach zu sylen (damits auch mobil gut aussieht)


??? example "nextcloud als dashboard"

    auch iframes,
    framing um nextcloud
    könnte auch noch mal exploriert werden

    pros:

      * sehr simpel

    cons:

      * nicht so hübsch


??? example "CSS Injection "yunohost""

    * bischen hacky, aber auch cool
    * funktioniert mit traefik nicht mehr?

??? example "browser plugin"

    pros:

      * schnell umzusetzen
      * könnte das link teilen problem vllt lösen
        (rewrite von .local-it.org domains)

    cons:

      * muss extra installiert werden
      * sogar vom store verifiziert werden
      * portabilität
        für mehrere browser unterschiedlich
      * mobil schwierig
      * security/privacy (plugin hat viel mehr rechte im browser)
        kann ein ungutes gefühl machen
      *

??? example "desktop / mobile app"

    vllt garnicht so schwer mit integrierter webapp


    pros:
      * leichtere zugänglichkeit / onboarding

    cons:
      * gleiches "link teilen" problem


Entscheidung
------------

Wir bauen ein Dashboard das Wekan und Nextcloud als Iframe einbindet, Nextcloud als Dashboard wird als variante nochmal betrachtet.
Parallel explorieren wir forks (Aufwand, technische Umsetzung).
