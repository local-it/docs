* Website: [local-it.org](https://local-it.org)
* Mail: [support@local-it.org](mailto:support@local-it.org)
* <button id="feedback-form">[Feedback-Formular]()</button>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script id="zammad_form_script" src="https://support.local-it.org/assets/form/form.js"></script>

<script>
$(function() {
  $('#feedback-form').ZammadForm({
    messageTitle: 'Feedback-Formular',
    messageSubmit: 'Übermitteln',
    messageThankYou: 'Vielen Dank für Ihre Anfrage (#%s). Wir melden uns umgehend.',
    modal: true,
    attachmentSupport: true  });
});
</script>
