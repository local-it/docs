---
status: draft
---

# Holsteincloud 3.0 2021

Durch regionale Fördergelder konnten die Stadtwerke Eutin 
uns beauftragen, pilothaft für zwei Vereine eine kollaborationsplattform 
einzuführen und zu betreiben. Darunter gab es die Hospiz Initative Euin und
die Freiwillige Feuerwehr Eutin, Fissau und Neubar . # TODO

### Hospizinitative Eutin

Kontakt entstand durch die Stadtwerke Eutin.
Verein mit hauptamtlichen und ehrenamtlichen.
hohes durchschnittsalter, gerine it-affinität

Nutzen bisher Whatsapp, E-Mail, Dropbox, Google Kalendar,
IT-Dienstleister für Website,

Haben Bedarf an Dateiaustausch für Dokumente, Bilder, gemeinsame Dokumentenbearbeitung, Projektmanagementtool für Feierlichkeit

25 User:innen


#### Ablauf


1. Auftakt Veranstaltung, Vorstellung, **Anforderungs Workshops**  
   Welche digitalen Tools möchte der Verein gerne nutzen?

2. Bereitstellung einer Pilotinstanz mit Authentik, Nextcloud, Wekan.  
  Verbindung per SSO
  Installation von Authentik, Nextcloud, Onlyoffice, Wekan
  Integration per openid connect
  automatisierung, erstellen von coopcloud recipes
  Integration von Backupbot

3. Einführungsworkshop in die Tools

4. Supportphase (Updates und Hilfe bei Anfragen) und regelmäßige Feedbackgespräche


#### Learnings

* viel Früher Feedback von Einzelnen aus Pilotgruppe einholen 
* Schulung mit kleinerer Gruppe machen
* Zur Bedarfsabfrage schon Demo-Zugang bereitstellen
* mehr Zeit mit Vereinen einplanen für Planung
* Nextcloud (Dateiverwaltung und Kalendar) kommt gut an und wird genutzt.
* Wekan wird nicht genutzt, weil es als zu kompliziert wahrgenommen wird.
  Kamen vorher von linearer Projektmanagement. Kennen die Kanban Arbeitsweise nicht
* Im Workshop auch Mobilnutzung vorstellen 
* Gruppenfunktionalität in Wekan fehlt
* Userbootstrapping noch schwierig
  Wenn User selbst anlegen, nicht einheitliche user-namen  
  Wegen openid Anbindung werden User erst beim ersten login an die Apps übertragen


### Feuerwehr

#### Ablauf

1.
2. Workshopreihe
  1. Beispielszenarien und Erster Input
  2. Admin Workshop, Userverwaltung und Inhalte erstellen
  3. Reale Nutzung und Multiplikatoren Schulung
3.
4.

#### Nutzung

* Wekan als Reperaturbuch vom hauptamtlichen Gerätewart
* Lehrsaalnutzung und Fahrzeugnutzung und Werkstattnutzung 
* Nextcloud als Dateiablage für Weiterbildungsunterlagen und Protokolle für den Vorstand
* gemeinsame Dokumentenbearbeitung für die Grundausbildung als Anwesenheitsliste und in der Ausbildung


Verbesserung von SSO in Wekan



#### Learning

* Haben es anderen Feuerwehren schon empfohlen!

##### Wekan
* leichte Start-Schwierigkeiten, wegen falschen Berechtigungen (konnten sie selbst klären)
* Frage gehabt wie Benachrichtigungssystem funktioniert

* LDAP 
* Nutzungsszenarien gemeinsam in Workshop erarbeiten
  Fast Organisationsberatung
* Direkt eigene Instanz bereitstellen und an produktiv
  Szenarien testen lassen
* Wartungsfenster planen und kommunizieren



Läuft Weiter mit Supportkontingent und 40€ Pro Instanz
Stadtwerke übernehmen 1. Level Support



# fragen

wie viele feedback gespräche haben stattgefunden und war das feedback so hilfreich?
  oder brauchen wir vllt auch noch andere feedback technik? users in the wild

warum eig kein bedarf an chat?

Warum hat hospiz eig noch google kalender auf homepage?

nutzt die hospiz auch onlyoffice?

